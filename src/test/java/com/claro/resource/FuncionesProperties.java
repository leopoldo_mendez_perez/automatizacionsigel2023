package com.claro.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JOptionPane;

public class FuncionesProperties {

	public String leerProperties(String propertie,int num) {
		Properties propiedades = new Properties();
		InputStream entrada = null;

		propiedades.getProperty(propertie);
		try {
			entrada = new FileInputStream(direccionProperties(num));
			propiedades.load(entrada);
			//propiedades.getProperty(propertie);
			entrada.close();
			return propiedades.getProperty(propertie);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			
			return null;
		}catch(IOException e) {
			e.printStackTrace();
			return null;
		}
	}



	private String direccionProperties(int i) {
		switch(i) {
		case 1:
			return "./src/test/java/propertiesSigel3/propertiesXpathSigel.properties";
		case 2:
			return "./src/test/java/propertiesSigel3/propertiesDataSigel.properties";	
		case 3:
			return "./src/test/java/propertiesSigel3/propertiesSystemSigel.properties";
		default:
			return null;
		}
	}
	
}

