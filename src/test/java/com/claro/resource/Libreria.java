package com.claro.resource;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

   public class Libreria {
	protected WebDriver driver;
	static int  xpath=1,data=2,system=3;
	
	public Libreria(WebDriver driver2) {
		driver=driver2;
	}

	public int getXpath() {
		return xpath;
	}

	public void setXpath(int xpath) {
		this.xpath = xpath;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public int getSystem() {
		return system;
	}

	public void setSystem(int system) {
		this.system = system;
	}

	public void sendKeys(String inputText, By locator) {
		try {
			driver.findElement(locator).sendKeys(inputText);
		} catch (Exception e) {
         System.out.println("Input No encontrado: "+locator);
		}
	}
	
	//Metodo que me ayuda a con interactura con elementos de tipo boton
	public void click(String locator) {
		try {
		driver.findElement(By.xpath(locator)).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error en: "+locator);
		}
	}
	public void sendKeysKey(String inputText, String locator, Keys k) {
		driver.findElement(By.xpath(locator)).sendKeys(inputText, k);
    }
	
	public void ClearAndSetText(String text, By locator) {
		WebElement element = driver.findElement(locator);

		Actions navigator = new Actions(driver);
		navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
				.sendKeys(Keys.BACK_SPACE).sendKeys(text).sendKeys(Keys.ENTER).perform();
		System.out.println("XPATH: "+ locator);
		System.out.println("Texto: "+ text);

	}

	public void existWhitParameter(String element, int a)  {

		driver.manage().timeouts().implicitlyWait(a, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath(element));
	}

}
