package clasesPrincipales;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.claro.resource.FuncionesProperties;

public class conexionChrome {
	private WebDriver driver;
	
	FuncionesProperties fp=new FuncionesProperties();
	public WebDriver ConexionChrome() {
		System.setProperty("webdriver.chrome.driver", fp.leerProperties("driver",3));
		driver=new ChromeDriver();
		driver.get(fp.leerProperties("Url", 3));
		driver.manage().window().maximize();
		return driver;
	}
}
