package clasesPrincipales;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.claro.resource.FuncionesProperties;
import com.claro.resource.Libreria;

public class CP02_Agregar_producto_al_carrito extends Libreria {
FuncionesProperties fp=new FuncionesProperties();
	public CP02_Agregar_producto_al_carrito(WebDriver driver2) {
		super(driver2);
		// TODO Auto-generated constructor stub
	}
    public void CP02_Agregar_producto_al_carrito() {
    sendKeysKey(fp.leerProperties("busquedaProducto",getData()),fp.leerProperties("InputSearch",getXpath()), Keys.ENTER);	
    click(fp.leerProperties("Button_AgregarCarrito", getXpath()));
    existWhitParameter(fp.leerProperties("Validar_Alerta", getXpath()),1000);
    existWhitParameter(fp.leerProperties("Validar_Item", getXpath()), 1000);
    System.out.println("***** CP02_Agregar_producto_al_carrito"+"*****");
    }
}
